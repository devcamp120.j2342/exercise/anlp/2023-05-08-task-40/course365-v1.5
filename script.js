/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
var gCoursesDB = {
    description: "This DB includes all courses in system",
    courses: [
        {
            id: 1,
            courseCode: "FE_WEB_ANGULAR_101",
            courseName: "How to easily create a website with Angular",
            price: 750,
            discountPrice: 600,
            duration: "3h 56m",
            level: "Beginner",
            coverImage: "images/courses/course-angular.jpg",
            teacherName: "Morris Mccoy",
            teacherPhoto: "images/teacher/morris_mccoy.jpg",
            isPopular: false,
            isTrending: true
        },
        {
            id: 2,
            courseCode: "BE_WEB_PYTHON_301",
            courseName: "The Python Course: build web application",
            price: 1050,
            discountPrice: 900,
            duration: "4h 30m",
            level: "Advanced",
            coverImage: "images/courses/course-python.jpg",
            teacherName: "Claire Robertson",
            teacherPhoto: "images/teacher/claire_robertson.jpg",
            isPopular: false,
            isTrending: true
        },
        {
            id: 5,
            courseCode: "FE_WEB_GRAPHQL_104",
            courseName: "GraphQL: introduction to graphQL for beginners",
            price: 850,
            discountPrice: 650,
            duration: "2h 15m",
            level: "Intermediate",
            coverImage: "images/courses/course-graphql.jpg",
            teacherName: "Ted Hawkins",
            teacherPhoto: "images/teacher/ted_hawkins.jpg",
            isPopular: true,
            isTrending: false
        },
        {
            id: 6,
            courseCode: "FE_WEB_JS_210",
            courseName: "Getting Started with JavaScript",
            price: 550,
            discountPrice: 300,
            duration: "3h 34m",
            level: "Beginner",
            coverImage: "images/courses/course-javascript.jpg",
            teacherName: "Ted Hawkins",
            teacherPhoto: "images/teacher/ted_hawkins.jpg",
            isPopular: true,
            isTrending: true
        },
        {
            id: 8,
            courseCode: "FE_WEB_CSS_111",
            courseName: "CSS: ultimate CSS course from beginner to advanced",
            price: 750,
            discountPrice: 600,
            duration: "3h 56m",
            level: "Beginner",
            coverImage: "images/courses/course-css.jpg",
            teacherName: "Juanita Bell",
            teacherPhoto: "images/teacher/juanita_bell.jpg",
            isPopular: true,
            isTrending: true
        },
        {
            id: 9,
            courseCode: "FE_WEB_WORDPRESS_111",
            courseName: "Complete Wordpress themes & plugins",
            price: 1050,
            discountPrice: 900,
            duration: "4h 30m",
            level: "Intermediate",
            coverImage: "images/courses/course-wordpress.jpg",
            teacherName: "Clevaio Simon",
            teacherPhoto: "images/teacher/clevaio_simon.jpg",
            isPopular: true,
            isTrending: false
        },
        {
            id: 10,
            courseCode: "FE_UIUX_COURSE_211",
            courseName: "Thinkful UX/UI Design Bootcamp",
            price: 950,
            discountPrice: 700,
            duration: "5h 30m",
            level: "Advanced",
            coverImage: "images/courses/course-uiux.jpg",
            teacherName: "Juanita Bell",
            teacherPhoto: "images/teacher/juanita_bell.jpg",
            isPopular: false,
            isTrending: false
        },
        {
            id: 11,
            courseCode: "FE_WEB_REACRJS_210",
            courseName: "Front-End Web Development with ReactJs",
            price: 1100,
            discountPrice: 850,
            duration: "6h 20m",
            level: "Advanced",
            coverImage: "images/courses/course-reactjs.jpg",
            teacherName: "Ted Hawkins",
            teacherPhoto: "images/teacher/ted_hawkins.jpg",
            isPopular: true,
            isTrending: true
        },
        {
            id: 12,
            courseCode: "FE_WEB_BOOTSTRAP_101",
            courseName: "Bootstrap 4 Crash Course | Website Build & Deploy",
            price: 750,
            discountPrice: 600,
            duration: "3h 15m",
            level: "Intermediate",
            coverImage: "images/courses/course-bootstrap.png",
            teacherName: "Juanita Bell",
            teacherPhoto: "images/teacher/juanita_bell.jpg",
            isPopular: true,
            isTrending: false
        },
        {
            id: 14,
            courseCode: "FE_WEB_RUBYONRAILS_310",
            courseName: "The Complete Ruby on Rails Developer Course",
            price: 2050,
            discountPrice: 1450,
            duration: "8h 30m",
            level: "Advanced",
            coverImage: "images/courses/course-rubyonrails.png",
            teacherName: "Claire Robertson",
            teacherPhoto: "images/teacher/claire_robertson.jpg",
            isPopular: false,
            isTrending: true
        }
    ]
};
const gBASE_URL = "https://630890e4722029d9ddd245bc.mockapi.io/api/v1";

/*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
$(document).ready(function () {
    onPageLoading();
})
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
// call ajax để lấy dữ liệu

function onPageLoading() {
    $.ajax({
        url: gBASE_URL + "/courses",
        type: 'GET',
        dataType: 'json',
        success: function (courseDB) {
            //tạo các khối khóa học
            createDivRecommend(courseDB);
            createDivPopular(courseDB);
            createDivTrending(courseDB);
        },
        error: function (xhr, status, error) {
            console.error(error);
        }
    });
}
/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
// function tạo khối recommended to you
// input: data course
// output 4 thẻ div card
function createDivRecommend(paramData) {
    var recommendedCourse = $("#recommended-to-you");
    // chỉ hiển thị tối đa 4 card
    var gCountCard = 0;
    // chạy vòng lập tạo card
    $.each(paramData, function (index, Courses) {
        gCountCard++;
        //tạo thẻ div col để phân các thẻ
        var colCardRecommend = $("<div>").addClass("col-md-3 card-deck");
        // tạo card chứa các dữ liệu
        var cardRecommend = $("<div>").addClass("card mb-4 rounded-3 shadow-sm");
        // gắn hình ảnh của khóa học
        var vImgCover = $("<img>").attr("src", Courses.coverImage);

        var vCardBody = $("<div>").addClass("card-body text-left");

        // điền thông tin khóa hoc
        var vCardTitle = $("<h6>").addClass("card-title text-primary").text(Courses.courseName);
        var vNewText = Courses.duration + " " + Courses.level;
        var vDuration = $("<h6>").addClass("card-text text-muted").append("<i class='far fa-clock'></i> " + " " + vNewText);
        // giá khóa học
        var vPriceDiscount = $("<span>").addClass("discount-price").text(+ Courses.discountPrice);
        var vPrice = $("<h6>").addClass("card-title").text("$" + Courses.price + " $");
        vPrice.append(vPriceDiscount);
        // tại footer chứa thông tin giáo viên
        var vCardFooter = $("<div>").addClass("card-footer");
        // tạo thẻ div chứa thông tin footer
        var vDivFooter = $("<div>").addClass("media d-flex justify-content-between align-items-center");
        var vImgTeacher = $("<img>").addClass("mr-3 rounded-circle teacher-image").attr("src", Courses.teacherPhoto);
        var vTeacherName = $("<p>").addClass("media-body text-left").text(Courses.teacherName);
        var vIconBookMark = $("<i>").addClass("ml-auto far fa-bookmark text-left icon-image")
        vDivFooter.append(vImgTeacher, vTeacherName, vIconBookMark);
        vCardFooter.append(vDivFooter);

        // hiển thị các div ra card
        vCardBody.append(vCardTitle, vDuration, vPrice);
        cardRecommend.append(vImgCover, vCardBody, vCardFooter);
        colCardRecommend.append(cardRecommend)
        recommendedCourse.append(colCardRecommend);
        //Nếu có hơn 4 card đề xuất thì thoát vòng lặp
        if (gCountCard === 4) {
            return false;
        }
    });
}
// function tạo khối most popular
// input: data course
// output: 4 thẻ div card
function createDivPopular(paramData) {
    var popularCourses = $("#most-popular-courses");
    // chỉ hiển thị tối đa 4 card
    gCountCard = 0;
    // chạy vòng lập tạo card
    $.each(paramData, function (index, Courses) {
        if (Courses.isPopular === true) {
            gCountCard++;
            //tạo thẻ div col để phân các thẻ
            var colCardPopular = $("<div>").addClass("col-md-3 card-deck");
            // tạo card chứa các dữ liệu
            var cardPopular = $("<div>").addClass("card mb-4 rounded-3 shadow-sm");
            // gắn hình ảnh của khóa học
            var vImgCover = $("<img>").attr("src", Courses.coverImage);

            var vCardBody = $("<div>").addClass("card-body text-left");

            // điền thông tin khóa hoc
            var vCardTitle = $("<h6>").addClass("card-title text-primary").text(Courses.courseName);
            var vNewText = Courses.duration + " " + Courses.level;
            var vDuration = $("<h6>").addClass("card-text text-muted").append("<i class='far fa-clock'></i> " + " " + vNewText);
            // giá khóa học
            var vPriceDiscount = $("<span>").addClass("discount-price").text(+ Courses.discountPrice);
            var vPrice = $("<h6>").addClass("card-title").text("$" + Courses.price + " $");
            vPrice.append(vPriceDiscount);
            // tại footer chứa thông tin giáo viên
            var vCardFooter = $("<div>").addClass("card-footer");
            // tạo thẻ div chứa thông tin footer
            var vDivFooter = $("<div>").addClass("media d-flex justify-content-between align-items-center");
            var vImgTeacher = $("<img>").addClass("mr-3 rounded-circle teacher-image").attr("src", Courses.teacherPhoto);
            var vTeacherName = $("<p>").addClass("media-body text-left").text(Courses.teacherName);
            var vIconBookMark = $("<i>").addClass("ml-auto far fa-bookmark text-left icon-image")
            vDivFooter.append(vImgTeacher, vTeacherName, vIconBookMark);
            vCardFooter.append(vDivFooter);

            // hiển thị các div ra card
            vCardBody.append(vCardTitle, vDuration, vPrice);
            cardPopular.append(vImgCover, vCardBody, vCardFooter);
            colCardPopular.append(cardPopular)
            popularCourses.append(colCardPopular);
        }
        //Nếu có hơn 4 card đề xuất thì thoát vòng lặp
        if (gCountCard === 4) {
            return false;
        }
    });
}

// function tạo khối trending
// input: data course
// output: 4 thẻ div card
function createDivTrending(paramData) {
    var trendingCourses = $("#trending-courses");
    // chỉ hiển thị tối đa 4 card
    gCountCard = 0;
    // chạy vòng lập tạo card
    $.each(paramData, function (index, Courses) {
        if (Courses.isTrending === true) {
            gCountCard++;
            //tạo thẻ div col để phân các thẻ
            var colCardTrending = $("<div>").addClass("col-md-3 card-deck");
            // tạo card chứa các dữ liệu
            var cardTrending = $("<div>").addClass("card mb-4 rounded-3 shadow-sm");
            // gắn hình ảnh của khóa học
            var vImgCover = $("<img>").attr("src", Courses.coverImage);

            var vCardBody = $("<div>").addClass("card-body text-left");

            // điền thông tin khóa hoc
            var vCardTitle = $("<h6>").addClass("card-title text-primary").text(Courses.courseName);
            var vNewText = Courses.duration + " " + Courses.level;
            var vDuration = $("<h6>").addClass("card-text text-muted").append("<i class='far fa-clock'></i> " + " " + vNewText);
            // giá khóa học
            var vPriceDiscount = $("<span>").addClass("discount-price").text(+ Courses.discountPrice);
            var vPrice = $("<h6>").addClass("card-title").text("$" + Courses.price + " $");
            vPrice.append(vPriceDiscount);
            // tại footer chứa thông tin giáo viên
            var vCardFooter = $("<div>").addClass("card-footer");
            // tạo thẻ div chứa thông tin footer
            var vDivFooter = $("<div>").addClass("media d-flex justify-content-between align-items-center");
            var vImgTeacher = $("<img>").addClass("mr-3 rounded-circle teacher-image").attr("src", Courses.teacherPhoto);
            var vTeacherName = $("<p>").addClass("media-body text-left").text(Courses.teacherName);
            var vIconBookMark = $("<i>").addClass("ml-auto far fa-bookmark text-left icon-image")
            vDivFooter.append(vImgTeacher, vTeacherName, vIconBookMark);
            vCardFooter.append(vDivFooter);

            // hiển thị các div ra card
            vCardBody.append(vCardTitle, vDuration, vPrice);
            cardTrending.append(vImgCover, vCardBody, vCardFooter);
            colCardTrending.append(cardTrending)
            trendingCourses.append(colCardTrending);
        }
        //Nếu có hơn 4 card đề xuất thì thoát vòng lặp
        if (gCountCard === 4) {
            return false;
        }
    });
}