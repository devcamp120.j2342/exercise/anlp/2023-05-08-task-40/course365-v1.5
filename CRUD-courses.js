"use strict";
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
// Biến toàn cục để lưu trữ id courses đang đc update or delete. Mặc định = 0;

const gBASE_URL = "https://624abe0dfd7e30c51c110ac6.mockapi.io/api/v1/";
const gCONTENT_TYPE = "application/json;charset=UTF-8";
var gCoursesId = 0;


// Biến mảng hằng số chứa danh sách tên các thuộc tính
var gNameCol = ["id", "courseCode", "courseName", "price", "discountPrice", "duration", "level",
    "coverImage", "teacherName", "teacherPhoto", "isPopular", "isTrending"];

// Biến mảng toàn cục định nghĩa chỉ số các cột tương ứng
/* Gán số thứ tự các cột */
const gCOURSES_ID_COL = 0;
const gCOURSES_CODE_COL = 1;
const gCOURSES_NAME_COL = 2;
const gPRICE_COL = 3;
const gDISCOUNT_COL = 4;
const gDURATION_COL = 5;
const gLEVEL_COL = 6;
const gCOVER_IMAGE_COL = 7;
const gTEACHER_NAME_COL = 8;
const gTEACHER_IMAGE_COL = 9;
const gTRENDING_COL = 10;
const gPOPULAR_COL = 11;
const gACTION_COL = 12;

// Biến toàn cục để hiển lưu STT
var gSTT = 1;
// Khai báo DataTable & mapping collumns
var gCoursesTable = $("#courses-table").DataTable({
    columns: [
        { data: gNameCol[gCOURSES_ID_COL] },
        { data: gNameCol[gCOURSES_CODE_COL] },
        { data: gNameCol[gCOURSES_NAME_COL] },
        { data: gNameCol[gPRICE_COL] },
        { data: gNameCol[gDISCOUNT_COL] },
        { data: gNameCol[gDURATION_COL] },
        { data: gNameCol[gLEVEL_COL] },
        { data: gNameCol[gCOVER_IMAGE_COL] },
        { data: gNameCol[gTEACHER_NAME_COL] },
        { data: gNameCol[gTEACHER_IMAGE_COL] },
        { data: gNameCol[gTRENDING_COL] },
        { data: gNameCol[gPOPULAR_COL] },
        { data: gNameCol[gACTION_COL] }
    ],
    columnDefs: [
        { // định nghĩa lại cột STT
            targets: gCOURSES_ID_COL,
            render: function () {
                return gSTT++;
            }
        },
        { // định nghĩa lại cột cover image
            targets: gCOVER_IMAGE_COL,
            render: function (data) {
                return '<img src="' + data + '" style="height: 100px; width:200px;">';
            }
        },
        { // định nghĩa lại cột teacher image
            targets: gTEACHER_IMAGE_COL,
            render: function (data) {
                return '<img src="' + data + '" style="height:100px; width:100px;">';
            }
        },
        { // định nghĩa lại cột action
            targets: gACTION_COL,
            defaultContent: ` 
        <button class='btn btn-sm btn-primary btn-edit'>Sửa</button>
        <button class='btn btn-sm  btn-danger btn-delete'>Xóa</button>
      `
        }
    ]
});

/*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
$(document).ready(function () {
    // thực hiện tải trang
    onPageLoading();
    // gán sự kiện cho hai nút sửa và xóa trong table
    $("#courses-table").on("click", ".btn-edit", function () {
        onBtnEditCoursesClick(this);
    });
    $("#courses-table").on("click", ".btn-delete", function () {
        onBtnDeleteCoursesClick(this);
    });
    // Gán sự kiện Create - Thêm mới courses
    $("#btn-add-courses").on("click", function () {
        onBtnAddNewCoursesClick();
    });
    // gán sự kiện cho nút Thêm courses (trên modal)
    $("#btn-create-courses").on("click", function () {
        onBtnCreateCoursesClick();
    });
    // gán sự kiện cho nút Update courses (trên modal)
    $("#btn-update-courses").on("click", function () {
        onBtnUpdatecoursesClick();
    });
    // Gán sự kiện cho nút confirm delete courses
    $("#btn-confirm-delete-courses").on("click", function () {
        onBtnConfirmDeleteCoursesClick();
    })
});

/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
// hàm thực thi khi trang được load
function onPageLoading() {
    console.log("Loading Page")
    getAllCoursess();
}
// Hàm xử lý sự kiện khi nút Sửa mới đc click
function onBtnEditCoursesClick(paramElement) {
    gCoursesId = getcoursesIdFromButton(paramElement);
    // hiện modal chi tiết courses
    $("#edit-courses-modal").modal("show");
    // ajax lấy dữ liệu theo id 
    $.ajax({
        url: gBASE_URL + "/courses/" + gCoursesId,
        type: "GET",
        success: function (paramRes) {
            // B4: xử lý front-end
            handleEditcoursesSuccess(paramRes);
        },
        error: function (paramErr) {
            console.log(paramErr.status);
        }
    });
}
// Hàm xử lý sự kiện khi nút Xóa mới đc click
function onBtnDeleteCoursesClick(paramElement) {
    $("#delete-confirm-modal").modal("show");
    gCoursesId = getcoursesIdFromButton(paramElement);
}
// Hàm xử lý sự kiện khi nút Thêm mới đc click
function onBtnAddNewCoursesClick() {
    // hiển thị modal trắng lên
    $("#create-courses-modal").modal("show");
}
// hàm xử lý sự kiện create courses modal click
function onBtnCreateCoursesClick() {
    // khai báo đối tượng chứa courses data
    var vCoursesObj = {
        courseCode: "",
        courseName: "",
        price: 0,
        discountPrice: 0,
        duration: "",
        level: "",
        coverImage: "",
        teacherName: "",
        teacherPhoto: "",
        isPopular: false,
        isTrending: false,
    };
    // B1: Thu thập dữ liệu
    getCreateCoursesData(vCoursesObj);
    // B2: Validate insert
    var vIsCoursesValidate = validateCoursesData(vCoursesObj);
    if (vIsCoursesValidate) {
        // B3: insert courses
        $.ajax({
            url: gBASE_URL + "/courses",
            type: "POST",
            contentType: gCONTENT_TYPE,
            data: JSON.stringify(vCoursesObj),
            success: function () {
                // B4: xử lý front-end
                handleInsertCoursesSuccess()
            },
            error: function (paramErr) {
                console.log(paramErr.status);
            }
        });
    }
}
// hàm xử lý sự kiện update courses trên modal click
function onBtnUpdatecoursesClick() {
    // khai báo đối tượng chứa courses data
    var vCoursesObj = {
        courseCode: "",
        courseName: "",
        price: 0,
        discountPrice: 0,
        duration: "",
        level: "",
        coverImage: "",
        teacherName: "",
        teacherPhoto: "",
        isPopular: false,
        isTrending: false,
    };
    // B1: Thu thập dữ liệu
    getUpdatecoursesData(vCoursesObj);
    // B2: Validate insert
    var vIsCoursesValidate = validateCoursesData(vCoursesObj);
    if (vIsCoursesValidate) {
        // B3: insert courses
        $.ajax({
            url: gBASE_URL + "/courses/" + gCoursesId,
            type: "PUT",
            contentType: gCONTENT_TYPE,
            data: JSON.stringify(vCoursesObj),
            success: function (paramRes) {
                // B4: xử lý front-end
                handleUpdatecoursesSuccess()
            },
            error: function (paramErr) {
                console.log(paramErr.status);
            }
        });
    }
}
// Hàm xử lý sự kiện delete courses modal click
function onBtnConfirmDeleteCoursesClick() {
    debugger
    "use strict";
    $.ajax({
        url: gBASE_URL+ "/courses/" + gCoursesId,
        type: "DELETE",
        success: function (paramRes) {
            // B4: xử lý front-end
            handleDeletecoursesSuccess();
        },
        error: function (paramErr) {
            console.log(paramErr.status);
        }
    });
}
/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
// hàm gọi api để lấy all danh sách courses đăng ký
function getAllCoursess() {
    $.ajax({
        url: gBASE_URL + "/courses",
        type: 'GET',
        dataType: 'json',
        success: function (paramcoursess) {
            console.log(paramcoursess);
            loadDataToCoursesTable(paramcoursess);
        },
        error: function (paramErr) {
            console.log(paramErr.status);
        }
    });
}
/** hàm load courses array to DataTable
* in: courses array
* out: courses table has data
*/
function loadDataToCoursesTable(paramCoursesArr) {
    gSTT = 1;
    gCoursesTable.clear();
    gCoursesTable.rows.add(paramCoursesArr);
    gCoursesTable.draw();
}
// hàm thu thập dữ liệu để create courses
function getCreateCoursesData(paramCoursesObj) {
    paramCoursesObj.courseCode = $("#input-course-code").val().trim();
    paramCoursesObj.courseName = $("#input-course-name").val().trim();
    paramCoursesObj.price = Number($("#input-price").val().trim());
    paramCoursesObj.discountPrice = Number($("#input-discount-price").val().trim());
    paramCoursesObj.duration = $("#input-duration").val().trim();
    paramCoursesObj.level = $("#select-level option:selected").val();
    paramCoursesObj.coverImage = $("#input-cover-image").val().trim();
    paramCoursesObj.teacherName = $("#input-teacher-name").val().trim();
    paramCoursesObj.teacherPhoto = $("#input-teacher-photo").val().trim();
    paramCoursesObj.isPopular = $('#isPopular').is(':checked');
    paramCoursesObj.isTrending = $('#isTrending').is(':checked');
}
// hàm validate data
function validateCoursesData(paramCoursesObj) {
    if (paramCoursesObj.courseCode.length < 10) {
        alert("Courses Code phải nhiều hơn 10 ký tự");
        return false;
    } else if (paramCoursesObj.courseName.length < 20) {
        alert("Courses Code phải nhiều hơn 20 ký tự");
        return false;
    } else if (isNaN(paramCoursesObj.price) || paramCoursesObj.price < 0) {
        alert("Price phải là số nguyên lớn hơn 0");
        return false;
    } else if (paramCoursesObj.discountPrice !== "") {
        if (isNaN(paramCoursesObj.discountPrice) || paramCoursesObj.discountPrice > paramCoursesObj.price
            || paramCoursesObj.discountPrice < 0) {
            alert("Discount Price phải là số nguyên, >= 0 và phải <= price");
            return false;
        }
    } else if (paramCoursesObj.level === "") {
        alert("Bạn cần chọn level");
        return false;
    }
    return true;
}
// hàm xử lý hiển thị front-end khi thêm courses thành công
function handleInsertCoursesSuccess() {
    alert("Thêm courses thành công!");
    getAllCoursess();
    resertCreatecoursesForm();
    $("#create-courses-modal").modal("hide");
}
// hàm xóa trắng form create courses
function resertCreatecoursesForm() {
    $("#input-course-code").val("");
    $("#input-course-name").val("");
    $("#input-price").val("");
    $("#input-discount-price").val("");
    $("#input-duration").val("");
    $("#select-level").val("");
    $("#input-cover-image").val("");
    $("#input-teacher-name").val("");
    $("#input-teacher-photo").val("");
    $('#isTrending').prop('checked', false);
    $('#isPopular').prop('checked', false);
}
// hàm dựa vào button detail (edit or delete) xác định đc id courses
function getcoursesIdFromButton(paramButton) {
    var vRowClick = $(paramButton).closest("tr");// Xác định hàng chứa nút chi tiết
    var vTable = $("#courses-table").DataTable(); //Tạo biến truy xuất đến bảng datatable
    var vDataRow = vTable.row(vRowClick).data()//Lấy dữ liệu của hàng chứa nút click
    return vDataRow.id;
}
// hàm xử lý hiển thị edit courses modal
function handleEditcoursesSuccess(paramCoursesObj) {
    $("#edit-course-code").val(paramCoursesObj.courseCode);
    $("#edit-course-name").val(paramCoursesObj.courseName);
    $("#edit-price").val(paramCoursesObj.price);
    $("#edit-discount-price").val(paramCoursesObj.discountPrice);
    $("#edit-duration").val(paramCoursesObj.duration);
    $("#select-edit-level").val(paramCoursesObj.level);
    $("#edit-cover-image").val(paramCoursesObj.coverImage);
    $("#edit-teacher-name").val(paramCoursesObj.teacherName);
    $("#edit-teacher-photo").val(paramCoursesObj.teacherPhoto);
    $('#edit-isTrending').prop('checked', paramCoursesObj.isTrending);
    $('#edit-isPopular').prop('checked', paramCoursesObj.isPopular);
    $('#btn-update-courses').data("courses-id",paramCoursesObj.id);
}
// hàm thu thập dữ liệu để create courses
function getUpdatecoursesData(paramCoursesObj) {
    paramCoursesObj.courseCode = $("#edit-course-code").val().trim();
    paramCoursesObj.courseName = $("#edit-course-name").val().trim();
    paramCoursesObj.price = Number($("#edit-price").val().trim());
    paramCoursesObj.discountPrice = Number($("#edit-discount-price").val().trim());
    paramCoursesObj.duration = $("#edit-duration").val().trim();
    paramCoursesObj.level = $("#select-edit-level option:selected").val();
    paramCoursesObj.coverImage = $("#edit-cover-image").val().trim();
    paramCoursesObj.teacherName = $("#edit-teacher-name").val().trim();
    paramCoursesObj.teacherPhoto = $("#edit-teacher-photo").val().trim();
    paramCoursesObj.isPopular = $('#edit-isPopular').is(':checked');
    paramCoursesObj.isTrending = $('#edit-isTrending').is(':checked');
}
// hàm xử lý hiển thị front-end khi update courses thành công
function handleUpdatecoursesSuccess() {
    alert("Update courses thành công!");
    getAllCoursess();
    $("#edit-courses-modal").modal("hide");
}
// Hàm xử lý hiển thị front-end khi xóa courses thành công
function handleDeletecoursesSuccess() {
    alert("Xóa courses thành công!");
    getAllCoursess();
    $("#delete-confirm-modal").modal("hide"); // Ẩn đi modal xóa
}

